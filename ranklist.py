import collections
def get_all_words(file_name):
    return [line.strip() for line in open(file_name)]
def convert_list_dict(student_list):
    dict = {}
    for student in student_list :
        s = student.split()
        key = s.pop(0)
        dict[key] = list(map(int , s))
    return dict
def mark_list(dict):
    sorted_marks = sorted(dict, key = dict.__getitem__, reverse = True)
    for k in sorted_marks :
        print("{} {}".format(k, dict[k]))
mark_list(convert_list_dict(get_all_words("file.txt")))